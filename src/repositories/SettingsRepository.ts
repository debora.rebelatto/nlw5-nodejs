import { Repository, EntityRepository } from "typeorm";
import { Settings } from "../entities/setting";

@EntityRepository(Settings)
class SettingsRepository extends Repository<Settings> {
  public async getSettings(): Promise<any> {
    const settings = await this.findOne({
      where: { id: 1 }
    });
    return settings;
  }
}

export { SettingsRepository };
