import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryColumn, ManyToMany, ManyToOne, JoinColumn } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { User } from './users';

@Entity("messages")
class Messages {
  @PrimaryColumn()
  id: string;

  @Column()
  text: string;

  @Column()
  admin_id: string;

  @Column()
  user_id: string;

  @JoinColumn({ name: 'user_id' })
  @ManyToOne(type => User)
  user: User;

  @CreateDateColumn()
  created_at: Date;

  constructor() {
    this.id = uuid();
  }
}

export { Messages };