import { Request, Response } from 'express';
import { SettingsServices } from '../services/SettingsServices';

class SettingsContoller {
  async create(request: Request, response: Response) {
    const { chat, username } = request.body;

    try{
      const settingsService = new SettingsServices();
      const settings = await settingsService.create({chat, username});
      return response.json(settings);
    } catch (error) {
      return response.status(400).json({error: error.message});
    }
  }
}

export { SettingsContoller };